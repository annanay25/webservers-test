## Changelog.txt

Notice the change in response headers in the two requests from Apache (in the first one) to Nginx (in the second one)
This shows that requests are passing through reverse proxy.

## Loadbalanced.log

Notice the difference in the responses. 
One server has "You are visiting a.com - 1" and the other has "You are visiting a.com - 2".
Signifies that load balancing has been added.

## Appropriate config files for nginx and apache have been added.
