## Host specific pattern matching

Checkout diff nginx.conf ../q3/nginx.conf to see that only requests for hosts a.com and b.com are allowed to pass.
